

# HelloWorld_GL2

HelloWorld Project without to create a group.

#GitLab入門 (1) 入門
https://qiita.com/kaizen_nagoya/items/e8b9f4f0619e317db862


GitHub入門 (1) 入門
https://qiita.com/kaizen_nagoya/items/b6f5b1532fd21fec79c8
と似たことをGitLabでやりながら、それぞれのシステムの特徴を理解し、
使い勝手のいい方法を探す。
大きく違うのは、グループかも。最初にグループを作るか、最初にプロジェクトを作るか。
口座(account)開設

Google, Twitter, Github, Bitbucket, SalesforceのIDがあれば、
gitlab0.png
なければ、「sign in」の横の「Register」(登録)を撰択。
gitlab0a.png
もくもくと入力してください。
グループの作成

画面の左上の「グループ」
を押すと、右上の方に
「新規グループ」というボタンが現れる。
gitlab1.png
helloworld_gl
という名前で、公開のグループを作成した。
https://gitlab.com/helloworld_gl/
gitlab2.png
gitlab3.png
https://gitlab.com/helloworld_gl/hello-world
プロジェクトの作成

グループを作らずに、プロジェクトを作ることができる。
Want to house several dependent projects under the same namespace? Create a group.
gitlab1a.png
gitlaba2.png
現在地点

いまここ
グループから作った方。
https://gitlab.com/helloworld_gl/hello-world
プロジェクトから作った方。
https://gitlab.com/kaizen_nagoya/helloworld_gl2